FROM cloudron/base:1.0.0

ARG METABASE_VERSION=0.35.3

RUN apt-get update && \
    apt-get install -y openjdk-11-jre-headless && \
    rm -rf /var/cache/apt /var/lib/apt/lists

RUN mkdir -p /app/code /app/pkg

RUN curl -SLf "https://downloads.metabase.com/v${METABASE_VERSION}/metabase.jar" > /app/code/metabase.jar && \
    chown -R cloudron:cloudron /app/code

RUN ln -s /app/data/plugins /app/code/plugins

ADD env start.sh /app/pkg/

RUN mkdir -p /app/data && chown -R cloudron:cloudron /app/data

WORKDIR /app/code

USER cloudron

CMD [ "/app/pkg/start.sh" ]
