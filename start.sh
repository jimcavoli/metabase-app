#!/bin/bash

set -eu

mkdir -p /app/data/plugins

if [[ ! -f /app/data/env ]]; then
  echo "=> Copying default configuration"
  cp /app/pkg/env /app/data/env

  echo "=> Setting encryption key"
  echo "export MB_ENCRYPTION_SECRET_KEY=\"$(openssl rand -base64 64)\"" >> /app/data/env
fi

echo "=> Loading configuration"
# These could be overridden by the end-user
export MB_LDAP_ENABLED="true"

# Load the end-user configuration
source /app/data/env

# These are non-overridable by the end-user to ensure a "working" cloudron app
## Disable in-app update checks
export MB_CHECK_FOR_UPDATES="false"
## Set the app URL
export CLOUDRON_APP_ORIGIN=${CLOUDRON_APP_ORIGIN}
## Configure LDAP
export MB_LDAP_HOST=${CLOUDRON_LDAP_SERVER}
export MB_LDAP_PORT=${CLOUDRON_LDAP_PORT}
export MB_LDAP_BIND_DN=${CLOUDRON_LDAP_BIND_DN}
export MB_LDAP_PASSWORD=${CLOUDRON_LDAP_BIND_PASSWORD}
export MB_LDAP_SECURITY="none"
export MB_LDAP_USER_BASE=${CLOUDRON_LDAP_USERS_BASE_DN}
export MB_LDAP_USER_FILTER="(&(objectclass=user)(|(username={login})(mail={login})))"
export MB_LDAP_GROUP_BASE=${CLOUDRON_LDAP_GROUPS_BASE_DN}
export MB_LDAP_GROUP_SYNC="true"
export MB_LDAP_GROUP_MAPPINGS="{\"memberof=cn=admins,${CLOUDRON_LDAP_GROUPS_BASE_DN}\": [2]}"
export MB_LDAP_ATTRIBUTE_EMAIL="mail"
export MB_LDAP_ATTRIBUTE_FIRSTNAME="givenName"
export MB_LDAP_ATTRIBUTE_LASTNAME="sn"
## Configure SMTP
export MB_EMAIL_FROM_ADDRESS=${CLOUDRON_MAIL_FROM}
export MB_EMAIL_SMTP_HOST=${CLOUDRON_MAIL_SMTP_SERVER}
export MB_EMAIL_SMTP_PORT=${CLOUDRON_MAIL_SMTP_PORT}
export MB_EMAIL_SMTP_SECURITY="none"
export MB_EMAIL_SMTP_USERNAME=${CLOUDRON_MAIL_SMTP_USERNAME}
export MB_EMAIL_SMTP_PASSWORD=${CLOUDRON_MAIL_SMTP_PASSWORD}
## Configure postgresql
export MB_DB_TYPE="postgres"
export MB_DB_HOST=${CLOUDRON_POSTGRESQL_HOST}
export MB_DB_PORT=${CLOUDRON_POSTGRESQL_PORT}
export MB_DB_USER=${CLOUDRON_POSTGRESQL_USERNAME}
export MB_DB_PASS=${CLOUDRON_POSTGRESQL_PASSWORD}
export MB_DB_DBNAME=${CLOUDRON_POSTGRESQL_DATABASE}
## Java options
export JAVA_OPTS=""
export JAVA_OPTS="$JAVA_OPTS -XX:+IgnoreUnrecognizedVMOptions"
export JAVA_OPTS="$JAVA_OPTS -Djava.awt.headless=true"
export JAVA_OPTS="$JAVA_OPTS -Dfile.encoding=UTF-8"
export JAVA_OPTS="$JAVA_OPTS -XX:-UseGCOverheadLimit"
export JAVA_OPTS="$JAVA_OPTS -XX:+UseConcMarkSweepGC"
export JAVA_OPTS="$JAVA_OPTS -XX:+CMSClassUnloadingEnabled"
export JAVA_OPTS="$JAVA_OPTS -XX:+UseCompressedOops"
export JAVA_OPTS="$JAVA_OPTS -XX:+UseCompressedClassPointers"

echo "=> Starting Metabase"
exec java -jar /app/code/metabase.jar
